export class Post {
  title: string;
  content: string;
  loveIts: number;
  created_at: Date;

  constructor(title: string, content: string) {
    this.title = title;
    this.content = content;
    this.loveIts = 0;
    this.created_at = new Date();
  }

  getTitle() {
    return this.title;
  }

  getContent() {
    return this.content;
  }

  getCreatedAt() {
    return this.created_at;
  }

  loveIt() {
    this.loveIts++;
  }

  dontLoveIt() {
    this.loveIts--;
  }

  isLoved() {
    if(this.loveIts < 0) {
      return -1;
    } else if(this.loveIts > 0) {
      return 1;
    } else {
      return 0;
    }
  }
}
